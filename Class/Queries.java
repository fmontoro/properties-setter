package Extra;


import java.io.IOException;
import java.util.Properties;

/*
 *
 * @author fmontoro
 * 
 */

public class Queries {
        
    private String query, querySentence,propertiesFile;
    private Properties queries;
    
    
    public void initQueriesFile() throws IOException{
            
            
            this.queries = new Properties();
            this.queries.load(getClass().getClassLoader().getResourceAsStream(this.getPropertiesFile()));

    }
    
    public void printQuery(){
            System.out.println(this.getQuerySentence());
    }
    
    
    public String getPropertiesFile() {

            return this.propertiesFile;		

    }

    public void setPropertiesFile(String propertiesFile) {
            this.propertiesFile = propertiesFile;
    }
    
    
    public String getQuerySentence() {

            return this.queries.getProperty(this.querySentence);		

    }

    public void setQuerySentence(String querySentence) {
            this.querySentence = querySentence;
    }
    
    public String getQuery() {
            return this.query;
    }
    
    public void setQuery(String query) {
            this.query = query;
    }
}
    
	
    	

    


    

   

    
    
    
    
    
    
    
    
    
    

