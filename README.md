                                                        A java class to get values of keys from properties files.




    setPropertiesFile(String propertiesFile)
    
    set the name of the property file that will be load. If it is in another class remember to specify it

    initQueriesFile() 
    
    load properties file

    setQuerySentence(String querySentence)
    
    set the key value from the properties file you want to get.

    getQuerySentence()
    
    gets the value of the key of the properties file.

    Instantiate the class. 


    Set the properties file and then init ... never do the other way round 


    Side note


    --In properties file


    key= this {0} {1} ......{N}  

    \ to read the next line

    --In class

    MessageFormat.format(this.getQuerySentence(),argument1....,argument n)